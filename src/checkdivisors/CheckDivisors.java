package checkdivisors;
import java.util.Scanner;

public class CheckDivisors {

    public static int divizori(int nr)
    {
        int c=2;
        for(int i=2; i<=nr/2; i++)
        {
            if( nr%i == 0)
            {
                c++;
            }
        }
        return 2*c;//nr total de divizori intregi
    }
    
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        int a,b,ad,bd;
        
        System.out.println("Primul numar: ");
        a=scanner.nextInt();
        System.out.println("Al doilea numar: ");
        b=scanner.nextInt();

        ad=divizori(a);
        bd=divizori(b);
        if( ad == bd )
            System.out.println("Cele 2 numere au acelasi numar total de divizori.");
        else
            System.out.println("Cele 2 numere nu au acelasi numar total de divizori");
       
    }
    
}
